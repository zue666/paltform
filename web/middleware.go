package web

type Middleware func(Handler) Handler

func wrapMiddleware(handler Handler, mw []Middleware) Handler {
	for _, h := range mw {
		if h != nil {
			handler = h(handler)
		}
	}

	return handler
}
