package web

import (
	"context"
	"log"
	"net/http"
	"os"
	"syscall"
	"time"

	"go.opencensus.io/plugin/ochttp/propagation/tracecontext"
	"go.opencensus.io/trace"

	"github.com/julienschmidt/httprouter"
)

type ctxKey int

const KeyValues ctxKey = 1

type Values struct {
	TraceID    string
	Now        time.Time
	StatusCode int
	Error      bool
}

type Handler func(ctx context.Context, log *log.Logger, w http.ResponseWriter, r *http.Request, params httprouter.Params) error

type App struct {
	*httprouter.Router
	shutdown chan os.Signal
	log      *log.Logger
	mw       []Middleware
}

func New(shutdown chan os.Signal, log *log.Logger, mw ...Middleware) *App {
	r := httprouter.New()
	r.HandleMethodNotAllowed = false
	// r.NotFound = notfound(log, mw)

	return &App{
		Router:   r,
		shutdown: shutdown,
		log:      log,
		mw:       mw,
	}
}

func (a *App) SignalShutdown() {
	a.log.Println("error returned from handler indicated integrity issue, shutting down service.")
	a.shutdown <- syscall.SIGSTOP
}

func (a *App) Handle(verb, path string, handler Handler, mw ...Middleware) {

	handler = wrapMiddleware(wrapMiddleware(handler, mw), a.mw)

	h := func(w http.ResponseWriter, r *http.Request, params httprouter.Params) {

		var hf tracecontext.HTTPFormat

		var ctx context.Context
		var span *trace.Span

		if sc, ok := hf.SpanContextFromRequest(r); ok {
			ctx, span = trace.StartSpanWithRemoteParent(r.Context(), "platform.web", sc)
		} else {
			ctx, span = trace.StartSpan(r.Context(), "platform.web")
		}

		defer span.End()

		v := Values{
			TraceID: span.SpanContext().TraceID.String(),
			Now:     time.Now(),
		}

		ctx = context.WithValue(ctx, KeyValues, &v)

		hf.SpanContextToRequest(span.SpanContext(), r)

		if err := handler(ctx, a.log, w, r, params); err != nil {
			a.log.Printf("******> critical shtudown error: %v", err)
			a.SignalShutdown()
			return
		}
	}

	a.Router.Handle(verb, path, h)
}

type shutdown struct {
	Message string
}

func (s *shutdown) Error() string {
	return s.Message
}

func Shutdown(message string) error {
	return &shutdown{Message: message}
}

func IsShutdown(err error) bool {
	if _, ok := err.(*shutdown); ok {
		return true
	}

	return false
}
