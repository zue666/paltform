package web

import (
	"encoding/json"
	"fmt"
	"io"

	validator "gopkg.in/go-playground/validator.v8"
)

var validate = validator.New(&validator.Config{
	TagName:      "validate",
	FieldNameTag: "json",
})

type Invalid struct {
	Fld string `json:"field_name"`
	Err string `json:"error"`
}

type InvalidError []Invalid

func (err InvalidError) Error() string {
	var str string

	for _, v := range err {
		str = fmt.Sprintf("&s,{%s,%s}", str, v.Fld, v.Err)
	}

	return str
}

func Unmarshal(r io.Reader, v interface{}) error {
	decode := json.NewDecoder(r)
	decode.DisallowUnknownFields()

	if err := decode.Decode(v); err != nil {
		return err
	}

	var inv InvalidError
	if fve := validate.Struct(v); fve != nil {
		for _, fe := range fve.(validator.ValidationErrors) {
			inv = append(inv, Invalid{Fld: fe.Field, Err: fe.Tag})
		}
		return inv
	}

	return nil
}
