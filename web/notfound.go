package web

import (
	"context"
	"log"
	"net/http"
	"time"

	"go.opencensus.io/plugin/ochttp/propagation/tracecontext"
	"go.opencensus.io/trace"
)

func notfound(log *log.Logger, mw []Middleware) http.Handler {

	// handler := wrapMiddleware(handler, mw)

	h := func(w http.ResponseWriter, r *http.Request) {

		var hf tracecontext.HTTPFormat

		var ctx context.Context
		var span *trace.Span

		if sc, ok := hf.SpanContextFromRequest(r); ok {
			ctx, span = trace.StartSpanWithRemoteParent(r.Context(), "platform.web", sc)
		} else {
			ctx, span = trace.StartSpan(r.Context(), "platform.web")
		}

		defer span.End()

		v := Values{
			TraceID: span.SpanContext().TraceID.String(),
			Now:     time.Now(),
		}

		ctx = context.WithValue(ctx, KeyValues, &v)

		hf.SpanContextToRequest(span.SpanContext(), r)

	}

	return http.HandlerFunc(h)
}
