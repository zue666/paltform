package web

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	"github.com/pkg/errors"
)

var (
	ErrNotHealthy   = errors.New("Not Healthy")
	ErrNotFound     = errors.New("Entity Not Found")
	ErrInvalidID    = errors.New("ID is not in its proper form")
	ErrValidation   = errors.New("Validation errors occurred")
	ErrUnauthorized = errors.New("Unauthorized")
	ErrForbidden    = errors.New("Forbidden")
)

type JSONError struct {
	Error  string       `json:"error"`
	Fields InvalidError `json:"fields,omitempty"`
}

func Error(ctx context.Context, log *log.Logger, w http.ResponseWriter, err error) {
	switch errors.Cause(err) {
	case ErrNotHealthy:
		Respond(ctx, log, w, JSONError{Error: err.Error()}, http.StatusInternalServerError)
		return

	case ErrNotFound:
		Respond(ctx, log, w, JSONError{Error: err.Error()}, http.StatusNotFound)
		return

	case ErrValidation:
		Respond(ctx, log, w, JSONError{Error: err.Error()}, http.StatusBadRequest)
		return

	case ErrUnauthorized:
		Respond(ctx, log, w, JSONError{Error: err.Error()}, http.StatusUnauthorized)
		return

	case ErrForbidden:
		Respond(ctx, log, w, JSONError{Error: err.Error()}, http.StatusForbidden)
		return
	}

	switch e := errors.Cause(err).(type) {
	case InvalidError:
		je := JSONError{Error: "field validation failure.", Fields: e}
		Respond(ctx, log, w, je, http.StatusBadRequest)
		return
	}

	Respond(ctx, log, w, JSONError{Error: err.Error()}, http.StatusInternalServerError)
}

func Respond(ctx context.Context, log *log.Logger, w http.ResponseWriter, data interface{}, code int) {

	v, ok := ctx.Value(KeyValues).(*Values)
	if !ok {
		Error(ctx, log, w, Shutdown("web value missing from context"))
		return
	}

	v.StatusCode = code

	if code == http.StatusNoContent || data == nil {
		w.WriteHeader(code)
		return
	}

	jsonData, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		log.Printf("%s: Respond %v Marshalling JSON response\n", v.TraceID, err)
		Error(ctx, log, w, err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(jsonData)
}
