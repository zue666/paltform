package middlewares

import (
	"context"
	"log"
	"net/http"
	"time"

	"bitbucket.org/zue666/platform/web"
	"github.com/julienschmidt/httprouter"
	"go.opencensus.io/trace"
)

func RequestLogger(before web.Handler) web.Handler {
	h := func(ctx context.Context, log *log.Logger, w http.ResponseWriter, r *http.Request, params httprouter.Params) error {
		ctx, span := trace.StartSpan(ctx, "platform.middlewares.RequestLogger")
		defer span.End()

		v, ok := ctx.Value(web.KeyValues).(*web.Values)

		if !ok {
			return web.Shutdown("web value missing from context")
		}

		err := before(ctx, log, w, r, params)

		log.Printf("%s : (%d) : %s %s -> %s (%s)",
			v.TraceID,
			v.StatusCode,
			r.Method, r.URL.Path,
			r.RemoteAddr, time.Since(v.Now),
		)

		return err
	}

	return h
}
