package middlewares

import (
	"context"
	"log"
	"net/http"
	"strings"

	"bitbucket.org/zue666/platform/web"
	"github.com/julienschmidt/httprouter"
	"github.com/pkg/errors"
	"go.opencensus.io/trace"
)

type ctxKey int

const Key ctxKey = 1

type Auth struct {
}

func (a *Auth) Authenticate(after web.Handler) web.Handler {
	h := func(ctx context.Context, log *log.Logger, w http.ResponseWriter, r *http.Request, params httprouter.Params) error {

		ctx, span := trace.StartSpan(ctx, "platform.middlewares.Authenticate")
		defer span.End()

		authHeader := r.Header.Get("Authorization")

		if authHeader == "" {
			return errors.Wrap(web.ErrUnauthorized, "missing authrization header")
		}

		tknStr, err := parseAuthHeader(authHeader)

		if err != nil {
			return errors.Wrap(web.ErrUnauthorized, err.Error())
		}

		ctx = context.WithValue(ctx, Key, tknStr)

		return after(ctx, log, w, r, params)
	}

	return h
}

func parseAuthHeader(tkn string) (string, error) {

	split := strings.Split(tkn, " ")

	if len(split) != 2 || strings.ToLower(split[0]) != "token" {
		return "", errors.New("Expected Authorization header format: Token <token>")
	}

	return split[1], nil
}
