package middlewares

import (
	"context"
	"expvar"
	"log"
	"net/http"
	"runtime"

	"bitbucket.org/zue666/platform/web"
	"github.com/julienschmidt/httprouter"
	"go.opencensus.io/trace"
)

var m = struct {
	gr  *expvar.Int
	req *expvar.Int
	err *expvar.Int
}{
	gr:  expvar.NewInt("goroutines"),
	req: expvar.NewInt("requests"),
	err: expvar.NewInt("errors"),
}

func Metrics(before web.Handler) web.Handler {

	h := func(ctx context.Context, log *log.Logger, w http.ResponseWriter, r *http.Request, params httprouter.Params) error {

		ctx, span := trace.StartSpan(ctx, "platform.middlewares.Metrics")
		defer span.End()

		v, ok := ctx.Value(web.KeyValues).(*web.Values)
		if !ok {
			return web.Shutdown("web value missing from context")
		}

		err := before(ctx, log, w, r, params)

		m.req.Add(1)

		if m.req.Value()%100 == 0 {
			m.gr.Set(int64(runtime.NumGoroutine()))
		}

		if v.Error {
			m.err.Add(1)
		}

		return err
	}

	return h
}
