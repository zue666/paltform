package middlewares

import (
	"context"
	"log"
	"net/http"
	"runtime/debug"

	"github.com/pkg/errors"

	"bitbucket.org/zue666/platform/web"
	"github.com/julienschmidt/httprouter"
	"go.opencensus.io/trace"
)

func ErrorHandler(before web.Handler) web.Handler {
	h := func(ctx context.Context, log *log.Logger, w http.ResponseWriter, r *http.Request, params httprouter.Params) error {
		ctx, span := trace.StartSpan(ctx, "platform.middlewares.ErrorHandler")
		defer span.End()

		v, ok := ctx.Value(web.KeyValues).(*web.Values)
		if !ok {
			return web.Shutdown("web value missing from request")
		}

		defer func() {
			if r := recover(); r != nil {

				v.Error = true

				log.Printf("%s: Error : Panic Caught : %s\n", v.TraceID, r)

				web.Error(ctx, log, w, errors.New("unhandled"))

				log.Printf("%s : ERROR : Stacktrace\n%s\n", v.TraceID, debug.Stack())

			}
		}()

		if err := before(ctx, log, w, r, params); err != nil {
			v.Error = true
			err = errors.Cause(err)
			if ok := web.IsShutdown(err); ok {
				web.Error(ctx, log, w, errors.New("unhandled"))
				return err
			}

			if err != web.ErrNotFound {
				log.Printf("%s: Error : %v\n", v.TraceID, err)
			}

			web.Error(ctx, log, w, err)
			return nil
		}
		return nil
	}

	return h
}
