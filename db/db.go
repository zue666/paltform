package db

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"    // gorm mysql driver
	_ "github.com/jinzhu/gorm/dialects/postgres" // gorm postgresql driver
)

// DB struct
type DB struct {
	DB    *gorm.DB
	Error error
}

// NewPSQL creates new postgesql instance
func NewPSQL(user, password, host, port, database, sslmode string) *DB {

	connectionString := fmt.Sprintf("host=%s port=%s sslmode=%s dbname=%s user=%s password=%s", host, port, sslmode, database, user, password)

	db, err := gorm.Open("postgres", connectionString)

	return &DB{
		DB:    db,
		Error: err,
	}
}

// NewMySQL creates new mysql instance
func NewMySQL(user, password, host, port, database string) *DB {

	connectionString := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", user, password, host, port, database)

	db, err := gorm.Open("mysql", connectionString)

	return &DB{DB: db, Error: err}

}

// Migrate the database tables.
func (db *DB) Migrate(v ...interface{}) {

	for _, m := range v {
		db.DB.AutoMigrate(m)
	}

}

// Close the database connection
func (db *DB) Close() {

	db.DB.Close()

}
